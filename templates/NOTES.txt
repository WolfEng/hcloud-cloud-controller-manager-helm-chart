Thank you for installing {{ .Chart.Name }}!

In case of problems regarding this Helm Chart please help us to improve:
  https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/

In case of general problems with hcloud-cloud-controller-manager you can get help here:
  https://github.com/hetznercloud/hcloud-cloud-controller-manager/
